const express = require('express');
const app = express();
const jade = require("jade");
const bodyParser = require('body-parser');
var Sequelize = require('sequelize');
const _ = require("underscore");
// Declaration of Database
var sequelize = new Sequelize('test','root','sana1996',{
  host: 'localhost',
  dialect: 'mysql',
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 40000
  }
});

 var Posts = sequelize.define('posts',{
    title:{
      type:Sequelize.STRING,
      field: 'title',
    },
    description:{
      type:Sequelize.STRING,
      field: "description"
    },
    email:{
      type:Sequelize.STRING,
      field: "email"
    }   
  },
  {
    timestamps:false
  });
 Posts.all = function(){
    return sequelize.query("SELECT * FROM posts ORDER BY TITLE DESC",
    {
      model:Posts,
      type: sequelize.QueryTypes.SELECT,
      raw:true
    })
  }
 Posts.add = function(title,description,email){
    return sequelize.query("INSERT INTO posts (title, description, email) VALUES (?, ?, ?) ;",
    {
      model:Posts,
      replacements:[
        title,
        description,
        email
      ],
      type: sequelize.QueryTypes.INSERT,
      raw:true
    })
  }

app.set('view engine', 'jade');
app.set("views", __dirname + "/src/templates");
app.use(bodyParser.json({limit:'5mb'})); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true, limit:'5mb' })); // for parsing application/x-www-form-urlencoded

app.get("/posts/submit",function(req, res){

	res.render('add',{});
});
app.get("/posts",function(req, res){
	Posts.all().then(function(posts){
		res.render('list',{posts:posts});
	});
});
app.post("/posts/submit",function(req, res){
	Posts.add(req.body.title, req.body.description, req.body.email)
	res.redirect("/posts");
});

app.listen(80);